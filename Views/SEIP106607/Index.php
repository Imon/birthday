<?php
use App\BITM\SEIP106607\Birthday\Birthday;
include_once ("../../vendor/autoload.php");

$Info = new Birthday();
$Infos = $Info->Index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Creating Birthday details</title>
    </head>
    <body>
        <a href="Create.php">Create New</a>
        <a href="../../index.php">Back to home</a> </br></br>
        <table border="1">
            <thead>
                <td>SL. No.</td>
                <td>Name</td>
                <td>Birthday</td>
                <td colspan="3" style="text-align: center">Action</td>
            </thead>
            <tbody>
                
                <?php
                $i=0;
                foreach ($Infos as $details){
                    $i++;
                
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $details['name'];?></td>
                    <td><?php echo $details['birthday'];?></td>
                    <td>View</td>
                    <td><form action="Update.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $details['id'];?>"/>
                                    <input type="submit" value="Edit"/>
                        </form></td>
                    <td>Delete</td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </body>
</html>



